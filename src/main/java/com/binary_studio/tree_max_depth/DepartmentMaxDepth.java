package com.binary_studio.tree_max_depth;

import java.util.Stack;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (rootDepartment == null) {
			return 0;
		}
		Stack<Department> departmentStack = new Stack<>();
		Stack<Integer> depthStack = new Stack<>();
		departmentStack.push(rootDepartment);
		depthStack.push(1);
		int currentDepth = 0;
		int maxDepth = Integer.MIN_VALUE;
		while (departmentStack.size() > 0) {
			Department root = departmentStack.pop();
			currentDepth = depthStack.pop();
			if (root != null) {
				maxDepth = Math.max(maxDepth, currentDepth);
				for (Department department : root.subDepartments) {
					departmentStack.add(department);
					depthStack.add(currentDepth + 1);
				}
			}
		}
		return maxDepth;
	}

}
