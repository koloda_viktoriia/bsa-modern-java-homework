package com.binary_studio.fleet_commander.core.subsystems;

import java.util.Objects;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private String name;

	private PositiveInteger pgRequirement;

	private PositiveInteger capacitorUsage;

	private PositiveInteger impactReduction;

	private PositiveInteger shieldRegen;

	private PositiveInteger hullRegen;

	public DefenciveSubsystemImpl(String name, PositiveInteger pgRequirement, PositiveInteger capacitorUsage,
			PositiveInteger impactReduction, PositiveInteger shieldRegen, PositiveInteger hullRegen) {

		this.name = name;
		this.pgRequirement = pgRequirement;
		this.capacitorUsage = capacitorUsage;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
	}

	public DefenciveSubsystemImpl(DefenciveSubsystemImpl subsystem) {

		this.name = subsystem.name;
		this.pgRequirement = subsystem.pgRequirement;
		this.capacitorUsage = subsystem.capacitorUsage;
		this.impactReduction = subsystem.impactReduction;
		this.shieldRegen = subsystem.shieldRegen;
		this.hullRegen = subsystem.hullRegen;
	}

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {

		if ((name == null) || (name.isEmpty()) || name.trim().length() == 0) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
		return new DefenciveSubsystemImpl(name, powergridConsumption, capacitorConsumption, impactReductionPercent,
				shieldRegeneration, hullRegeneration);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {

		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {

		return this.capacitorUsage;
	}

	@Override
	public String getName() {

		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {

		Integer percentDamage;
		if (this.impactReduction.value() >= 95) {
			percentDamage = 95;
		}
		else {
			percentDamage = this.impactReduction.value();
		}

		Integer damage = incomingDamage.damage.value();
		damage = Math.toIntExact((long) Math.ceil(damage.doubleValue() - percentDamage / 100. * damage));

		AttackAction reduceDamage = new AttackAction(PositiveInteger.of(damage), incomingDamage.attacker,
				incomingDamage.target, incomingDamage.weapon);
		return reduceDamage;
	}

	@Override
	public RegenerateAction regenerate() {

		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

	@Override
	public boolean equals(Object o) {

		if (this == o) {
			return true;
		}
		if (!(o instanceof DefenciveSubsystemImpl)) {
			return false;
		}
		DefenciveSubsystemImpl that = (DefenciveSubsystemImpl) o;
		return Objects.equals(getName(), that.getName()) && Objects.equals(this.pgRequirement, that.pgRequirement)
				&& Objects.equals(this.capacitorUsage, that.capacitorUsage)
				&& Objects.equals(this.impactReduction, that.impactReduction)
				&& Objects.equals(this.shieldRegen, that.shieldRegen) && Objects.equals(this.hullRegen, that.hullRegen);
	}

	@Override
	public int hashCode() {

		return Objects.hash(getName(), this.pgRequirement, this.capacitorUsage, this.impactReduction, this.shieldRegen,
				this.hullRegen);
	}

}
