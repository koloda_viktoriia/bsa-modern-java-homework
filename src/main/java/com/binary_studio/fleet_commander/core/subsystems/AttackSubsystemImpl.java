package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

public final class AttackSubsystemImpl implements AttackSubsystem {

	private String name;

	private PositiveInteger pgRequirement;

	private PositiveInteger capacitorUsage;

	private PositiveInteger optimalSpeed;

	private PositiveInteger optimalSize;

	private PositiveInteger baseDamage;

	public AttackSubsystemImpl(String name, PositiveInteger pgRequirement, PositiveInteger capacitorUsage,
			PositiveInteger optimalSpeed, PositiveInteger optimalSize, PositiveInteger baseDamage) {

		this.name = name;
		this.pgRequirement = pgRequirement;
		this.capacitorUsage = capacitorUsage;
		this.optimalSpeed = optimalSpeed;
		this.optimalSize = optimalSize;
		this.baseDamage = baseDamage;
	}

	public AttackSubsystemImpl(AttackSubsystemImpl subsystem) {

		this.name = subsystem.name;
		this.pgRequirement = subsystem.pgRequirement;
		this.capacitorUsage = subsystem.capacitorUsage;
		this.optimalSpeed = subsystem.optimalSpeed;
		this.optimalSize = subsystem.optimalSize;
		this.baseDamage = subsystem.baseDamage;
	}

	public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
			PositiveInteger capacitorConsumption, PositiveInteger optimalSpeed, PositiveInteger optimalSize,
			PositiveInteger baseDamage) throws IllegalArgumentException {

		if ((name == null) || (name.isEmpty()) || name.trim().length() == 0) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}

		return new AttackSubsystemImpl(name, powergridRequirments, capacitorConsumption, optimalSpeed, optimalSize,
				baseDamage);
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {

		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {

		return this.capacitorUsage;
	}

	@Override
	public PositiveInteger attack(Attackable target) {

		Double sizeReductionModifier;
		Double speedReductionModifier;
		if (target.getSize().value() >= this.optimalSize.value()) {
			sizeReductionModifier = 1.;
		}
		else {
			sizeReductionModifier = (double) target.getSize().value() / this.optimalSize.value();
		}
		if (target.getCurrentSpeed().value() <= this.optimalSpeed.value()) {
			speedReductionModifier = 1.;
		}
		else {
			speedReductionModifier = (double) this.optimalSpeed.value() / (2 * target.getCurrentSpeed().value());
		}
		Integer damage = Math.toIntExact((long) Math
				.ceil(this.baseDamage.value().doubleValue() * Math.min(sizeReductionModifier, speedReductionModifier)));
		PositiveInteger attack = new PositiveInteger(damage);
		return attack;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
