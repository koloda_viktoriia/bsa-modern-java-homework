package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger pg;

	private PositiveInteger capacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystemImpl attackSubsystem;

	private DefenciveSubsystemImpl defenciveSubsystem;

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger pg,
			PositiveInteger capacitor, PositiveInteger capacitorRegeneration, PositiveInteger speed,
			PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.pg = pg;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.speed = speed;
		this.size = size;
	}

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {
		return new DockedShip(name, shieldHP, hullHP, powergridOutput, capacitorAmount, capacitorRechargeRate, speed,
				size);
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.attackSubsystem = null;
		}
		else {
			if (this.pg.value() < subsystem.getPowerGridConsumption().value()) {
				Integer missingPG = subsystem.getPowerGridConsumption().value() - this.pg.value();
				throw new InsufficientPowergridException(missingPG);
			}
			else {
				this.attackSubsystem = new AttackSubsystemImpl((AttackSubsystemImpl) subsystem);
				this.pg = new PositiveInteger(this.pg.value() - subsystem.getPowerGridConsumption().value());
			}
		}

	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (subsystem == null) {
			this.defenciveSubsystem = null;
		}
		else {
			if (this.pg.value() < subsystem.getPowerGridConsumption().value()) {
				Integer missingPG = subsystem.getPowerGridConsumption().value() - this.pg.value();
				throw new InsufficientPowergridException(missingPG);
			}
			else {
				this.defenciveSubsystem = new DefenciveSubsystemImpl((DefenciveSubsystemImpl) subsystem);
				this.pg = new PositiveInteger(this.pg.value() - subsystem.getPowerGridConsumption().value());
			}
		}

	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (this.defenciveSubsystem == null && this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		else if (this.defenciveSubsystem == null) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (this.attackSubsystem == null) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else {
			return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.pg, this.capacitor,
					this.capacitorRegeneration, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
		}

	}

}
