package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntitityImpl;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.AttackSubsystemImpl;
import com.binary_studio.fleet_commander.core.subsystems.DefenciveSubsystemImpl;

public final class CombatReadyShip implements CombatReadyVessel {

	private String name;

	private PositiveInteger maxShieldHP;

	private PositiveInteger shieldHP;

	private PositiveInteger maxHullHP;

	private PositiveInteger hullHP;

	private PositiveInteger pg;

	private PositiveInteger capacitor;

	private PositiveInteger freeCapacitor;

	private PositiveInteger capacitorRegeneration;

	private PositiveInteger speed;

	private PositiveInteger size;

	private AttackSubsystemImpl attackSubsystem;

	private DefenciveSubsystemImpl defenciveSubsystem;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger pg,
			PositiveInteger capacitor, PositiveInteger capacitorRegeneration, PositiveInteger speed,
			PositiveInteger size, AttackSubsystemImpl attackSubsystem, DefenciveSubsystemImpl defenciveSubsystem) {
		this.name = name;
		this.maxShieldHP = shieldHP;
		this.shieldHP = shieldHP;
		this.maxHullHP = hullHP;
		this.hullHP = hullHP;
		this.pg = pg;
		this.capacitor = capacitor;
		this.freeCapacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
	}

	@Override

	public void endTurn() {
		this.freeCapacitor = PositiveInteger.of(this.freeCapacitor.value() + this.capacitorRegeneration.value());
		if (this.freeCapacitor.value() > this.capacitor.value()) {
			this.freeCapacitor = this.capacitor;
		}
	}

	@Override
	public void startTurn() {

	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		if (this.freeCapacitor.value() < this.attackSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {

			this.freeCapacitor = PositiveInteger
					.of(this.freeCapacitor.value() - this.attackSubsystem.getCapacitorConsumption().value());
			Integer damage = this.attackSubsystem.attack(target).value();
			NamedEntity attacker = new NamedEntitityImpl(this.getName());
			NamedEntity target_name = new NamedEntitityImpl(target.getName());
			NamedEntity weapon = new NamedEntitityImpl(this.attackSubsystem.getName());
			AttackAction attack = new AttackAction(new PositiveInteger(damage), attacker, target, weapon);
			return Optional.of(attack);
		}

	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		// this.freeCapacitor=
		// PositiveInteger.of(Math.max(this.freeCapacitor.value()-this.attackSubsystem.getCapacitorConsumption().value(),0));
		Integer damage = this.defenciveSubsystem.reduceDamage(attack).damage.value();
		if (damage >= this.shieldHP.value()) {
			this.hullHP = PositiveInteger.of(Math.max(this.hullHP.value() + this.shieldHP.value() - damage, 0));
			this.shieldHP = PositiveInteger.of(0);
		}
		else {
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() - damage);
		}
		NamedEntity target_name = new NamedEntitityImpl(attack.target.getName());
		NamedEntity weapon = new NamedEntitityImpl(this.attackSubsystem.getName());
		if (this.hullHP.value() == 0) {
			return new AttackResult.Destroyed();
		}
		else {
			return new AttackResult.DamageRecived(weapon, PositiveInteger.of(damage), target_name);
		}

	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		if (this.freeCapacitor.value() < this.defenciveSubsystem.getCapacitorConsumption().value()) {
			return Optional.empty();
		}
		else {

			this.freeCapacitor = PositiveInteger
					.of(this.freeCapacitor.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
			RegenerateAction regenerate = this.defenciveSubsystem.regenerate();
			Integer regShield = regenerate.shieldHPRegenerated.value();
			Integer regHull = regenerate.hullHPRegenerated.value();
			if ((regShield + this.shieldHP.value()) > this.maxShieldHP.value()) {
				regShield = this.maxShieldHP.value() - this.shieldHP.value();
			}
			if ((regHull + this.hullHP.value()) > this.maxHullHP.value()) {
				regHull = this.maxHullHP.value() - this.hullHP.value();
			}
			this.hullHP = PositiveInteger.of(this.hullHP.value() + regHull);
			this.shieldHP = PositiveInteger.of(this.shieldHP.value() + regShield);
			return Optional.of(new RegenerateAction(PositiveInteger.of(regShield), PositiveInteger.of(regHull)));

		}
	}

}
