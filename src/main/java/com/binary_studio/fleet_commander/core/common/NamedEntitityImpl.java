package com.binary_studio.fleet_commander.core.common;

public class NamedEntitityImpl implements NamedEntity {

	private final String name;

	public NamedEntitityImpl(String name) {
		this.name = name;
	}

	@Override
	public String getName() {
		return this.name;
	}

}
