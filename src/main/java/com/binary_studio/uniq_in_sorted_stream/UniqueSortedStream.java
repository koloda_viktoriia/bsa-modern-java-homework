package com.binary_studio.uniq_in_sorted_stream;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> key) {

		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(key.apply(t));

	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {

		return stream.filter(distinctByKey(Row::getPrimaryId));

	}

}
